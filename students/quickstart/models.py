from django.db import models

# Create your models here.

class Grade(models.Model):
    grade = models.CharField(max_length=5)
    class Meta:
        db_table = 'grades'

class Student(models.Model):
    fullname = models.CharField(max_length=50)
    grade = models.ForeignKey(Grade, db_column='gradeid', on_delete=models.DO_NOTHING)
    class Meta:
        db_table = 'students'