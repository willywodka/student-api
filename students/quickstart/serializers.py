from rest_framework import serializers
from quickstart.models import Student, Grade
from django.db.models import Q

class GradeSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    grade = serializers.CharField(max_length=5,required=False)

class StudentSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    fullname = serializers.CharField(max_length=50)
    grade = GradeSerializer()

    def validate_grade(self, value):
        #print(value)

        if 'id' in value and 'grade' in value:
            if Grade.objects.using('students').filter(Q(id__exact=value['id']) & Q(grade__exact=value['grade'])):
                return value
            else:
                raise serializers.ValidationError('Grade object is not exists in grade list')
        elif 'id' in value:
            if Grade.objects.using('students').filter(id__exact=value['id']):
                return value
            else:
                raise serializers.ValidationError('Grade id is not exists in grade list')
        elif 'grade' in value:
            if Grade.objects.using('students').filter(grade__exact=value['grade']):
                return value
            else:
                raise serializers.ValidationError('Grade is not exists in grade list')
        else:
            raise serializers.ValidationError('Grade field is Required')

    def create(self, validated_data):
        
        if 'id' in validated_data and 'grade' in validated_data:
            finalstudent = Student(fullname=validated_data['fullname'], grade=validated_data['grade'])
        elif 'id' in validated_data['grade']:
            grade = Grade.objects.using('students').filter(id__exact=validated_data['grade']['id']).first()
            finalstudent = Student(fullname=validated_data['fullname'], grade=grade)
        elif 'grade' in validated_data['grade']:
            grade = Grade.objects.using('students').filter(grade__exact=validated_data['grade']['grade']).first()
            finalstudent = Student(fullname=validated_data['fullname'], grade=grade)

        finalstudent.save()

        return finalstudent

    def update(self, instance, validated_data):
        instance.fullname = validated_data.get('fullname', instance.fullname)

        if 'id' in validated_data and 'grade' in validated_data:
            instance.grade = validated_data.get('grade', instance.grade)
        elif 'id' in validated_data['grade']:
            grade = Grade.objects.using('students').filter(id__exact=validated_data['grade']['id']).first()
            instance.grade = grade
        elif 'grade' in validated_data['grade']:
            grade = Grade.objects.using('students').filter(grade__exact=validated_data['grade']['grade']).first()
            instance.grade = grade

        instance.save()
        
        return instance

