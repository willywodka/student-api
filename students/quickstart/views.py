from django.http import JsonResponse, HttpResponse#, Response
from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework.utils import json
from rest_framework.parsers import JSONParser
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from quickstart.models import Student, Grade
from quickstart.serializers import StudentSerializer, GradeSerializer
# Create your views here.

@api_view(["GET"])
def GradeView(request):
    if request.method == 'GET':
        grade = Grade.objects.using('students').all()
        serializer = GradeSerializer(grade , many=True)
        return JsonResponse(serializer.data, safe=False)

@api_view(["GET", "PUT", "DELETE"])
def StudentViewWithId(request, pk):

    if request.method == 'GET':
        try:
            student = Student.objects.using('students').get(pk=pk)
            serializer = StudentSerializer(student)
            return JsonResponse(serializer.data)
        except Student.DoesNotExist:
            return Response({'message':'Student not found with id: '+pk}, status=status.HTTP_404_NOT_FOUND)
    elif request.method == 'PUT':
        try:
            student = Student.objects.using('students').get(pk=pk)
            serializer = StudentSerializer(student,data=request.data)

            if serializer.is_valid(raise_exception=True):
                serializer.save()
                return JsonResponse(serializer.data)
        except Student.DoesNotExist:
            return Response({'message':'Student not found with id: '+pk}, status=status.HTTP_404_NOT_FOUND)
    elif request.method == 'DELETE':
        try:
            student = Student.objects.using('students').get(pk=pk)
            student.delete()
            return Response(data={'message':'Student was deleted successfully'})
        except Student.DoesNotExist:
            return Response({'message':'Student not found with id: '+pk}, status=status.HTTP_404_NOT_FOUND)


@api_view(["GET", "POST"])
def StudentView(request):

    if request.method == 'GET':
        students = Student.objects.using('students').all()
        serializer = StudentSerializer(students, many=True)
        return JsonResponse(serializer.data, safe=False)
    
    elif request.method == 'POST':
        serializer = StudentSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response({'message':'Invalid field'})
