"""students URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from rest_framework import permissions
from django.conf.urls.static import static
from django.conf import settings

from drf_yasg.views import get_schema_view
from drf_yasg import openapi

#from rest_framework.schemas import get_schema_view
#from rest_framework_swagger.views import get_swagger_view
#from rest_framework.documentation import include_docs_urls 
from quickstart import views

schema_view = get_schema_view(
    openapi.Info(
        title="Students API",
        default_version='v1',
        description="Api for students",
    ),
    permission_classes=(permissions.AllowAny,),
    public=True,
)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('grades/', views.GradeView),
    path('students/', views.StudentView),
    url(r'^swagger(?P<format>.json|.yaml)$', schema_view.without_ui(cache_timeout=None), name='schema-json'),
    url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=None), name='schema-swagger-ui'),
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=None), name='schema-redoc'),
    url(r'^students/(?P<pk>[0-9]+)$', views.StudentViewWithId)
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
